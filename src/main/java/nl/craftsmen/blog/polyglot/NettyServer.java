package nl.craftsmen.blog.polyglot;

import org.glassfish.jersey.netty.httpserver.NettyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

public class NettyServer {
    public static void main(String[] args) {
        var resourceConfig = ResourceConfig.forApplicationClass(JerseyApplication.class);
        var server = NettyHttpContainerProvider.createHttp2Server(URI.create("http://localhost:8081/"), resourceConfig, null);
        Runtime.getRuntime().addShutdownHook(new Thread(server::close));
    }
}
