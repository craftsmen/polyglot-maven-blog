package nl.craftsmen.blog.polyglot.controller;

import nl.craftsmen.blog.polyglot.api.Task;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("tasks")
@Produces(APPLICATION_JSON)
public class TaskController {
    private final Map<UUID, Task> tasks = new HashMap<>();

    public TaskController() {
        createTask(new Task(
                UUID.randomUUID(),
                "Write a blog on polyglot maven",
                false,
                OffsetDateTime.of(2019, 7, 31, 12, 0, 0, 0, ZoneOffset.ofHours(2)))
        );
    }

    @GET
    public Collection<Task> getTasks() {
        return tasks.values();
    }

    @GET
    @Path("expired")
    public Collection<Task> getExpiredTasks() {
        return tasks.values().stream().filter(task -> task.getDueDate().isAfter(OffsetDateTime.now())).collect(Collectors.toList());
    }

    @GET
    @Path("{id}")
    public Task getTask(@PathParam("id") UUID id) {
        return tasks.get(id);
    }

    @POST
    public void createTask(Task task) {
        tasks.put(task.getId(), task);
    }

    @PUT
    @Path("{id}")
    public void updateTask(@PathParam("id") UUID id, Task task) {
        tasks.remove(id);
        tasks.put(task.getId(), task);
    }

    @DELETE
    @Path("{id}")
    public Task deleteTask(@PathParam("id") UUID id) {
        return tasks.remove(id);
    }
}
