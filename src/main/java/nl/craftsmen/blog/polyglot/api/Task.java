package nl.craftsmen.blog.polyglot.api;

import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.UUID;

public class Task {
    private final UUID id;
    private final String description;
    private final boolean done;
    private final OffsetDateTime dueDate;

    public Task(UUID id, String description, boolean done, OffsetDateTime dueDate) {
        this.id = id;
        this.description = description;
        this.done = done;
        this.dueDate = dueDate;
    }

    public UUID getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDone() {
        return done;
    }

    public OffsetDateTime getDueDate() {
        return dueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(id, task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", done=" + done +
                ", dueDate=" + dueDate +
                '}';
    }
}
