package nl.craftsmen.blog.polyglot;

import nl.craftsmen.blog.polyglot.controller.TaskController;

import javax.ws.rs.core.Application;
import java.util.Set;

public class JerseyApplication extends Application {
    @Override
    public Set<Object> getSingletons() {
        return Set.of(new TaskController());
    }
}
